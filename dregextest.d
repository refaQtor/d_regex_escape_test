//rdmd -main -unittest -J. dregextest.d
module dregextest;

import std.string;

void dregextest(ref string byte_string)
{
    import std.regex;
    string[string] fixes;
    
    fixes[" !.*[\r\n]*"] = "\n"; //remove remarks, keep remark removal in this order        
    fixes["!.*[\r\n]*"] = ""; //remove header, keep remark removal in this order
    fixes["[\r\n]*\n"] = "\n";
    fixes["\n[\\s]*\n"] = "\n"; //remove multiple blank lines 
    
    fixes["\\\\Field "] = "\\field ";
    fixes["\\\\Default "] = "\\default ";
    fixes["\\\\Note "] = "\\note ";
    fixes[",[ ]*\\\\[Nn]ote .*\n"] = ",\n";
    fixes[",\\\\[Nn]ote .*\n"] = ",\n"; //and the spaceless version
    fixes[";[ ]*\\\\[Nn]ote .*\n"] = ";\n";
    fixes[";\\\\[Nn]ote .*\n"] = ";\n"; //and the spaceless version
    
    foreach (key; fixes.keys)
    {
        byte_string = byte_string.replaceAll(regex(key, "gim"), fixes[key]); 
        //unfortunately, the "case insensitive" regex switch is broken in phobos
    }
}

unittest
{
    import std.stdio, std.file, std.algorithm;
// getting files from disk directly, so not further confused 
// by escaped characters in local strings
    static auto dirty_string = import ("dregextest.txt");

    writeln(dirty_string , "---");
    
    dregextest(dirty_string);
    
    assert(!dirty_string.canFind("\\Default"), "############ Default");
    assert(!dirty_string.canFind("\\Field"), "############ Field");
    assert(!dirty_string.canFind("\\Note"), "############ Note");
    
    static auto correct_string = import ("dregextest_correct.txt");
    
    writeln(dirty_string);
    
    assert(dirty_string == correct_string, "############ String Match"); 
}
